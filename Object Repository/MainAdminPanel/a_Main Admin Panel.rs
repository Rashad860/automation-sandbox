<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Main Admin Panel</name>
   <tag></tag>
   <elementGuidId>411f56c2-9a04-4e3d-a2c2-54247d4b9759</elementGuidId>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/div[@class=&quot;navbar-right&quot;]/ul[@class=&quot;nav navbar-nav nav-drop nav-right-drop&quot;]/li[@class=&quot;dropdown pull-right open&quot;]/ul[@class=&quot;dropdown-menu&quot;]/li[4]/a[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://qa-1.coredev.cloud/708646210/corelims?cmd=list-superTypes&amp;entityType=SUPERTYPE</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Main Admin Panel</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-coreat_nav</name>
      <type>Main</type>
      <value>item</value>
   </webElementProperties>
</WebElementEntity>
