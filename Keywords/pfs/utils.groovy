package pfs
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData

import com.kms.katalon.core.testobject.ConditionType

public class utils {
	
	def static getObject(String xPath){
		return new CoreTestObject().addProperty("xpath", ConditionType.EQUALS, xPath)
	}
	
	def static getXPath(String inDb, String inText){
		for (cell in findTestData(inDb).getAllData()) {
			if(cell[0] == inText){
				return cell[1]
			}
		}
		return null
	}
		
	def static getXPathParamArray(String inDb, String inText, List<String> arrStr){
		println(arrStr[1])
		int counter = 1
		for (cell in findTestData(inDb).getAllData()) {
			if(cell[0] == inText){
				String dataToParse = cell[1]
				for(repString in arrStr){
					dataToParse = dataToParse.replace('$' + counter.toString(), repString)
					counter++
				}
				return dataToParse
			}
		}
		return null
	}
	
	def static buildObject(String inDb, String inText){
		getObject(getXPath(inDb,inText))
	}
	
	def static buildObjectParamArray(String inDb, String inText, List<String> arrayParam){
		getObject(getXPathParamArray(inDb,inText,arrayParam))
	}
}
