package pfs

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class entity {
	static String objDb = 'entity-xpaths'
	public static CoreTestObject actionMenu = utils.buildObject(entity.objDb,'actionMenu')
	public static CoreTestObject action(List<String> paramArray){
		 utils.getObject(actionMenu.XPath() + utils.getXPathParamArray(objDb,'action',paramArray))
	}
	public static class viewattributesassociations {
		public static class attributeassociatedmenu{
			public static CoreTestObject attributeAssociated = utils.buildObject(global.objDb,'attributeAssociated')
			public static CoreTestObject attributeItemContainer(List<String> paramArray){
				 utils.buildObjectParamArray(entity.objDb,'attributeItemContainer',paramArray)
			}
			public static CoreTestObject attributeItemAction(String inAttribute, List<String> paramArray){
				
				utils.getObject(attributeItemContainer(inAttribute).XPath() + 
					utils.getXPathParamArray(entity.objDb,'attributeItemAction',paramArray))
			}
		}
		public static class supertypeassociatedmenu{
			public static CoreTestObject superTypesAssociated = utils.buildObject(global.objDb,'superTypesAssociated')
			public static CoreTestObject superTypeItemContainer(List<String> paramArray){
				 utils.getObject(superTypesAssociated.XPath() + utils.getXPathParamArray(objDb,'superTypeItemContainer',paramArray))
			}
			public static CoreTestObject superTypeItemAction(String inAttribute, List<String> paramArray){
				utils.getObject(superTypeItemContainer(inAttribute).XPath() + utils.getXPathParamArray(objDb,'superTypeItemAction',paramArray))
			}
		}
	}
	public static class newentitytype{
		public static CoreTestObject chkbxEnforceAttributeSecurity = utils.buildObject(entity.objDb,'chkbxEnforceAttributeSecurity')
		public static CoreTestObject chkbxEnforceProjectSecurity = utils.buildObject(entity.objDb,'chkbxEnforceProjectSecurity')
		public static CoreTestObject chkbxPromptLocation = utils.buildObject(entity.objDb,'chkbxPromptLocation')
		public static CoreTestObject chkbxRequireSignature = utils.buildObject(entity.objDb,'chkbxRequireSignature')
		public static CoreTestObject chkbxTrackLocation = utils.buildObject(entity.objDb,'chkbxTrackLocation')
		public static CoreTestObject chkbxTrackVersion = utils.buildObject(entity.objDb,'chkbxTrackVersion')
		public static CoreTestObject chkbxUserBarcode = utils.buildObject(entity.objDb,'chkbxUserBarcode')
		public static CoreTestObject cmbDefaultLocation = utils.buildObject(entity.objDb,'cmbDefaultLocation')
		public static CoreTestObject txtBarCode = utils.buildObject(entity.objDb,'txtBarCode')
		public static CoreTestObject txtEntityName = utils.buildObject(entity.objDb,'txtEntityName')
		public static CoreTestObject txtQueryPageSize = utils.buildObject(entity.objDb,'txtQueryPageSize')
		public static CoreTestObject txtStartSequence = utils.buildObject(entity.objDb,'txtStartSequence')
	}
	public static class listallentitytypes {
		public static class entitytypenavigation{
			public static CoreTestObject entityTypeRow(List<String> paramArray){
				 utils.buildObjectParamArray(entity.objDb,'entityTypeRow',paramArray)
			}
			public static CoreTestObject btnShowActions(List<String> paramArray){
				utils.getObject(entityTypeRow(paramArray).XPath() + utils.getXPath(entity.objDb,'btnShowActions'))
			}
			public static CoreTestObject btnEntityAction(List<String> entityTypeParamArray, List<String> actionParamArray){
				utils.getObject(entityTypeRow(entityTypeParamArray).XPath() + utils.getXPathParamArray(entity.objDb,'btnEntityAction',actionParamArray))
			}
			public static class actionChain{
				public static navigate(List<String> entityRow, List<String> entityAction){
					if(entity.listallentitytypes.entitytypenavigation.btnEntityAction(entityRow,entityAction).waitForVisible() == false){
						entity.listallentitytypes.entitytypenavigation.btnShowActions(entityRow).click()
					}
					entity.listallentitytypes.entitytypenavigation.btnEntityAction(entityRow,entityAction).click()
				}
			}
		}
		public static class filter{
			public static CoreTestObject btnLetter(List<String> paramArray){
				utils.buildObjectParamArray(entity.objDb,'btnLetter',paramArray)
			}
			public static CoreTestObject txtFilter = utils.buildObject(entity.objDb,'txtFilter')
		}
	}
}


