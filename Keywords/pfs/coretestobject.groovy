package pfs

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class CoreTestObject extends TestObject {
	void setText(String inText){
		if(inText != null){
			WebUI.setText(this, inText)
		}
	}
	
	void click(){
		WebUI.click(this)
	}
	
	void doubleClick(){
		WebUI.doubleClick(this)
	}
	
	void select(String inText, String regEx="*"){
		if(inText != null){
			WebUI.selectOptionByLabel(this, inText + regEx, true)
		}
	}
	Boolean waitForVisible(){
		WebUI.waitForElementVisible(this, 5)
	}
	String XPath(){
		this.findPropertyValue("xpath")
	}
}
