package pfs

import com.kms.katalon.core.testobject.ConditionType

public class login {
	static String objDb = 'login-xpaths'
	public static CoreTestObject txt_username = utils.buildObject(objDb, 'txt_username')
	public static CoreTestObject txt_password = utils.buildObject(objDb, 'txt_password')
	public static CoreTestObject btn_login = utils.buildObject(objDb, 'btn_login')
	public static CoreTestObject cmb_tenant = utils.buildObject(objDb, 'cmb_tenant')
	public static CoreTestObject btn_continue = utils.buildObject(objDb, 'btn_continue')
	public static class actionChain{
		public static void singleTenant(String username, String password){
			login.txt_username.setText(username)
			login.txt_password.setText(password)
			login.btn_login.click()
		}
		public static void multiTenant(String username, String password, String tenant){
			login.txt_username.setText(username)
			login.txt_password.setText(password)
			login.btn_login.click()
			login.cmb_tenant.select(tenant, '( |\\t|\\n|\\f)*')
			login.btn_continue.click()
		}
	}
}
