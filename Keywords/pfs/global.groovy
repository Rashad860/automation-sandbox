package pfs

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class global {
	static String objDb = 'global-xpaths'
	public static class appMenu{
		public static CoreTestObject btn_Apps = utils.buildObject(global.objDb, 'btn_Apps')
		public static CoreTestObject containerApp = utils.getObject(btn_Apps.XPath() + utils.getXPath(global.objDb, 'containerApp'))
		public static link_App(List<String> paramArray){ 
			return utils.getObject(containerApp.XPath() + utils.getXPathParamArray(global.objDb,'link_App',paramArray))
		}
		public static class actionChain{
			public static navigate(String linkText){
				global.appMenu.btn_Apps.click()
				global.appMenu.link_App([linkText]).click()
			}
		}
	}
	public static class menu{
		public static CoreTestObject entity(List<String> paramArray){ 
			return utils.getObject(utils.getXPathParamArray(global.objDb,'entity',paramArray))
		}
		public static CoreTestObject entityType(List<String> paramArray){
			return utils.getObject(utils.getXPathParamArray(global.objDb,'entityType',paramArray))
		}
		public static CoreTestObject entityTypeLink(List<String> entityTypeParamArray, List<String> navEntityLinkParamArray){
			return utils.getObject(entityType(entityTypeParamArray).XPath() + utils.getXPathParamArray(global.objDb,'entityTypeLink',navEntityLinkParamArray))
		}
		public static class actionChain{
			public static navigate(String entity, String entityType, String linkText){
				global.menu.entity([entity]).click()
				global.menu.entityType([entityType]).click()
				global.menu.entityTypeLink([entityType], [linkText]).click()
			}
		}
	}
	public static class helpMenu{
		public static CoreTestObject btn_Help = utils.buildObject(global.objDb, 'btn_Help')
		public static CoreTestObject containerHelp = utils.getObject(btn_Help.XPath() + utils.getXPath(global.objDb, 'containerHelp'))
		public static CoreTestObject helpLink(List<String> paramArray){
			return utils.getObject(containerHelp.XPath() + utils.getXPathParamArray(global.objDb,'helpLink',paramArray))
		}
		public static class actionChain{
			public static navigate(String linkText){
				global.helpMenu.btn_Help.click()
				global.helpMenu.helpLink([linkText]).click()
			}
		}
	}
	public static class quickSearch{
		public static CoreTestObject btnSearch = utils.buildObject(global.objDb,'btnSearch')
		public static CoreTestObject btnLocation = utils.buildObject(global.objDb,'btnLocation')
		public static CoreTestObject treeLocations = utils.buildObject(global.objDb,'treeLocations')
		public static CoreTestObject txtBarcodeSearch = utils.buildObject(global.objDb,'txtBarcodeSearch')
		public static CoreTestObject childCell(List<String> paramArray){
			utils.getObject(treeLocations.XPath() + utils.getXPathParamArray(global.objDb,'childCell',paramArray))
		}
		public static locationSearch(String searchString){
			def values = searchString.split('/')
			btnLocation.click()
			values.each {
				childCell([it]).doubleClick()
			}
		}
		public static class actionChain{
			public static barCodeNavigate(String barCode){
				global.quickSearch.txtBarcodeSearch.setText(barCode)
				global.quickSearch.btnSearch.click()
			}
			public static locationNavigate(String inPath){
				global.quickSearch.locationSearch(inPath)
				global.quickSearch.btnSearch.click()
			}
			
		}
	}
	public static class userMenu{
		public static CoreTestObject btnUser = utils.buildObject(global.objDb,'btnUser')
		public static CoreTestObject userMenu = utils.getObject(btnUser.XPath() + utils.getXPath(global.objDb, 'userMenu'))
		public static CoreTestObject userMenuLink(List<String> paramArray){
			utils.getObject(userMenu.XPath() + utils.getXPathParamArray(global.objDb,'userMenuLink',paramArray))
		}
		public static class actionChain{
			public static navigate(String to){
				global.userMenu.btnUser.click()
				global.userMenu.userMenuLink([to]).click()
			}
		}
	}
}
